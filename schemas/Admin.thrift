namespace java org.cweb.schemas.admin

include "Identity.thrift"
include "Keys.thrift"
include "Storage.thrift"

struct StorageProfileRequest {
  1: required binary id
  2: required Keys.PublicKey publicKey
  3: optional string fromNickname
}

struct StorageProfileResponse {
  1: required Storage.PrivateStorageProfile privateStorageProfile
  2: required Identity.IdentityReference admin
}

struct LocalHostedProfile {
  1: required StorageProfileRequest request
  2: required Storage.PrivateStorageProfile privateStorageProfile
  3: required Identity.IdentityReference identityRef
  4: required i64 sharedAt
  5: optional i64 firstSeenAt
}

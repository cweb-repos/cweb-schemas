namespace java org.cweb.schemas.properties

include "Wire.thrift"

union PropertyValue {
  1: string str
  2: binary bin
  3: Wire.TypedPayload typedPayload
}

struct Property {
  1: required string key
  3: required PropertyValue value
}

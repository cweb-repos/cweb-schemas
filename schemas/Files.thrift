namespace java org.cweb.schemas.files

include "Properties.thrift"

struct FilePartDescriptor {
  1: required binary fileName
  2: required i64 startOffset
  3: required i64 contentLength
  4: optional binary contentHash
  5: required binary encryptionKey
  6: required string encryptionAlgorithm
}

struct FileMetadata {
  1: required string name
  2: required i64 createdAt
  3: required i64 size
  4: required list<Properties.Property> properties = []
}

struct FileContentDescriptor {
  1: required FileMetadata metadata
  20: required list<FilePartDescriptor> parts
  40: optional binary inlineContent
}

struct FileReference {
  1: required binary fromId
  2: required binary fileId
  3: required binary key
  4: optional FileMetadata metadata
}

struct LocalFileDownloadState {
  1: required binary fromId
  2: required binary fileId
  3: required string localFileName
  4: optional i64 startedAt
  5: optional i64 completedAt
  6: optional i64 abortedAt
  7: optional string lastError
  8: required i32 numConsecutiveErrors = 0
  20: list<i32> downloadedParts = []
}

struct EnqueuedDownload {
  1: required binary fromId
  2: required binary fileId
}

struct DownloadQueueState {
  1: required list<EnqueuedDownload> queue;
}

struct LocalUploadedFileInfo {
  1: required FileReference fileReference
}

struct LocalSharedFileInfo {
  1: required FileReference fileReference
}

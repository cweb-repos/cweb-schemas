namespace java org.cweb.schemas.identity

include "Properties.thrift"
include "Keys.thrift"
include "Storage.thrift"
include "Crypto.thrift"
include "Wire.thrift"

struct IdentityDescriptor {
  1: required binary id
  4: string protocolVersion
  7: Keys.PublicKey rsaPublicKey
  8: Keys.PublicKey ecPublicKey
  2: binary idProofEnvelope
  3: list<Properties.Property> ownProperties
  10: list<binary> endorsementSignedEnvelopes = []
  5: required Storage.PublicStorageProfile storageProfile
  6: Storage.PrivateBroadcastConfig privateBroadcastConfig
  9: list<Crypto.X3DHPreKeyBundle> x3dhPreKeyBundles = []
}

struct IdentityReference {
  1: required binary id
  2: required Storage.PublicStorageProfile storageProfile
}

struct LocalIdentityDescriptorState {
  1: required i64 fetchedAt;
  2: optional i64 signedAt;
  3: optional i64 validUntil;
  20: required IdentityDescriptor descriptor;
}

struct IdentityProfile {
  1: required list<Properties.Property> properties = []
}

struct LocalIdentityProfileStateOwn {
  1: required binary objectId
}

struct LocalIdentityProfileRemote {
  1: required binary fromId
  2: required binary profileObjectId
}

namespace java org.cweb.schemas.storage

include "Wire.thrift"

struct PublicStorageProfile {
  2: optional PublicS3StorageProfile publicS3StorageProfile
}

struct PublicS3StorageProfile {
  1: required string host
  2: required string region
  3: required string bucket
  4: required string pathPrefix
  7: S3Credentials readCredentials
}

struct PrivateStorageProfile {
  2: optional PrivateS3StorageProfile privateS3StorageProfile
}

struct S3Credentials {
  1: required string accessKeyId
  2: required string secretAccessKey
}

struct PrivateS3StorageProfile {
  1: required PublicS3StorageProfile publicS3StorageProfile
  2: S3Credentials credentials
}

struct PrivateBroadcastConfig {
  1: required i32 bucketNameBytes
  2: required i32 slotsPerBucket
}

struct LocalMetadataEnvelope {
  1: required Wire.TypedPayloadMetadata typedPayloadMetadata
  20: optional binary customMetadata
}

struct RemoteSyncStatus {
  1: optional i64 lastSuccessTime
  2: optional i64 lastAttemptTime
  3: optional string lastError
  4: required i32 numConsecutiveErrors = 0
}

struct InboundDataRecord {
  1: required RemoteSyncStatus fetchStatus
  2: optional i64 dataSize
  20: optional binary data
}

struct OutboundDataRecord {
  1: optional i64 deleteAt
  2: optional LocalMetadataEnvelope localMetadataEnvelope
  3: required RemoteSyncStatus uploadStatus
  4: required bool deleted = false
  5: optional RemoteSyncStatus deletionStatus
  6: required i64 dataSize
  7: required bool deleteDataAfterUpload = false
  20: optional binary data
}

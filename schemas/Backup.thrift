namespace java org.cweb.schemas.backup

struct BackupMetadata {
  1: required binary id
  20: optional binary applicationData
}

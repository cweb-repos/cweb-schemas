namespace java org.cweb.schemas.wire

//////////////////////////////// CRYPTO /////////////////////////////////
struct SignatureMetadata {
  1: optional binary recipientId
  2: required binary signerId
  3: required string signingAlgorightm
  4: optional i64 generatedAt
  5: optional i64 validUntil
}

struct SignedPayload {
  6: required SignatureMetadata metadata
  20: required binary payload
}

struct SignedEnvelope {
  1: required binary signature
  20: required binary signedPayload
}

struct PKEncryptedEnvelope {
  1: required binary pkEncryptedKey
  3: required string keyEncryptionAlgorithm
  4: required string payloadEncryptionAlgorithm
  20: required binary payload
}

struct SymmetricEncryptedEnvelope {
  1: string payloadEncryptionAlgorightm
  2: binary keyHash
  20: required binary payload
}

//////////////////////////////// TYPED PAYLOAD /////////////////////////////////
enum CompressionType {
  NONE = 0
  GZIP = 1
  DEFLATE = 2
}

enum PayloadType {
  CUSTOM = 1
  IDENTITY_DESCRIPTOR = 2
  IDENTITY_PROFILE = 3
  ENDORSEMENT_REQUEST = 5
  ENDORSEMENT_RESPONSE = 6
  COMM_SESSION_SEED = 7
  FILE_REFERENCE = 8
  ENDORSEMENT_PAYLOAD = 9
  FILE_CONTENT_DESCRIPTOR = 10
  SHARED_SESSION_DESCRIPTOR = 11
  SHARED_SESSION_MESSAGE = 12
  SHARED_SESSION_SYNC_MESSAGE = 13
  SHARED_OBJECT = 14
  SHARED_OBJECT_SYNC_MESSAGE = 15
  COMM_SESSION_SEED_CRYPTO_ENVELOPE = 16
  STORAGE_PROFILE_REQUEST = 17
  STORAGE_PROFILE_RESPONSE = 18
}

struct TypedPayloadMetadata {
  1: required PayloadType type
  2: optional string serviceName
  3: optional string customType
  4: optional binary refId
}

struct TypedPayload {
  1: required TypedPayloadMetadata metadata
  2: required CompressionType compression
  20: required binary data
}

//////////////////////////////// CRYPTO ENVELOPE /////////////////////////////////
union CryptoEnvelopeContent {
  1: TypedPayload typedPayload
  2: SignedEnvelope signedEnvelope
  3: PKEncryptedEnvelope pkEncryptedEnvelope
  4: SymmetricEncryptedEnvelope symmetricEncryptedEnvelope
}

struct CryptoEnvelope {
  20: required CryptoEnvelopeContent content
}

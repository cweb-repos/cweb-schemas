namespace java org.cweb.schemas.comm.shared

include "Properties.thrift"
include "Wire.thrift"
include "Crypto.thrift"
include "Identity.thrift"
include "Storage.thrift"

struct SharedSessionParticipantInfo {
  1: required Identity.IdentityReference identityRef
  2: required i32 joinedVersion
  3: optional i32 leftVersion
}

struct SharedSessionDescriptor {
  1: required binary sessionId
  2: required Identity.IdentityReference adminIdentityRef
  3: required i32 version
  4: required binary contentKey
  6: required i64 createdAt
  7: required list<Properties.Property> properties = []
  20: required list<SharedSessionParticipantInfo> participants
}

struct SharedSessionMessageMetadata {
  1: required binary sessionId
  2: required i32 descriptorVersion
  3: required binary fromId
  4: required binary messageIdSeed
  5: required binary messageId
  6: optional i64 createdAt
}

struct SharedSessionMessage {
  1: required SharedSessionMessageMetadata metadata
  2: required list<SharedSessionMessageMetadata> previousMessageMetadata
  21: required Wire.TypedPayload payload
}

struct SharedSessionSyncMessage {
  3: list<SharedSessionMessageMetadata> messageReferences
  4: optional Identity.IdentityReference adminIdentityRef
}

struct LocalSharedSessionPreviousData {
  1: required i32 descriptorVersion
  2: required binary contentKey
}

struct SharedSessionSyncStats {
  1: i32 syncMessagesSent = 0
  2: i32 syncMessagesReceived = 0
  3: i32 descriptorsReceived = 0
  4: i32 messagesFetched = 0
  5: i32 previousMessagesFetched = 0
  6: i64 lastDescriptorFetchTime = 0
  7: i64 lastMessageFetchTime = 0
  8: i32 prevTailsAdded = 0
  9: i32 prevTailsRemoved = 0
}

enum LocalSharedSessionRecentPeerType {
  SYNC = 1
  MESSAGE_RECEIVED = 2
}

struct LocalSharedSessionRecentPeer {
  1: required LocalSharedSessionRecentPeerType type
  2: required i64 time
  3: required binary id;
}

struct LocalSharedSessionState {
  1: required binary sessionId
  2: required SharedSessionSyncStats syncStats
  4: optional i64 lastDescriptorUpdatedAt
  5: optional i32 unsubscribedDescriptorVersion;
  20: SharedSessionDescriptor descriptor
  22: required list<SharedSessionMessageMetadata> messageFetchQueue
  23: required list<SharedSessionMessageMetadata> previousMessageTails
  24: required list<LocalSharedSessionPreviousData> previousData
  25: required list<binary> unconsumedMessageIds
  26: required list<binary> unconsumedAckedMessageIds = []
  27: required list<LocalSharedSessionRecentPeer> recentPeers = []
}

struct LocalSharedSessionMessage {
  1: required SharedSessionMessageMetadata messageReference
  2: required i64 fetchScheduledAt
  3: optional binary sourceSyncFromId
  4: optional binary sourcePrevInMessageId
  5: optional i64 fetchedAt
  6: optional i64 consumedAt
  7: optional i64 ackedAt
  20: optional SharedSessionMessage message
}

namespace java org.cweb.schemas.comm.object

include "Wire.thrift"

enum SharedObjectDeliveryType {
  DELIVER_ALL = 1
  DELIVER_LAST = 2
}

struct SharedObject {
  1: required binary fromId
  2: required binary objectId
  3: required SharedObjectDeliveryType type
  4: required i64 pollInterval
  5: required i64 createdAt
  20: required Wire.TypedPayload payload
}

struct SharedObjectReference {
  1: required binary fromId
  2: required binary objectIdSeed
  3: required binary objectId
  4: required binary key
  5: optional i32 lastVersionOfPreviousKey
}

struct SharedObjectUnsubscribedMessage {
  1: required binary objectId
}

struct SharedObjectSyncMessage {
  1: optional SharedObjectReference reference
  2: optional SharedObjectUnsubscribedMessage unsubscribedMessage
}

struct LocalSharedObjectPreviousData {
  1: required binary key
}

struct SharedObjectSubscriberInfo {
  1: required binary id
}

struct LocalSharedObjectStateOwn {
  1: required binary objectIdSeed
  2: required i64 objectTtl
  3: required i64 keyTtl
  4: required i64 currentKeyCreatedAt
  5: required binary currentKey
  6: required i32 nextVersionForCurrentKey
  7: required i64 currentObjectPublishedAt = 0
  20: required SharedObject object
  21: required list<SharedObjectSubscriberInfo> subscribers
}

struct LocalSharedObjectStateRemote {
  1: required SharedObjectReference currentReference
  2: optional i64 unsubscribedTime
  3: required i32 nextVersionForCurrentKey
  4: i64 lastReceivedAt
  20: SharedObject object
  23: required list<SharedObjectSyncMessage> syncQueue = []
  22: required list<Wire.TypedPayload> unconsumedPastObjects
}

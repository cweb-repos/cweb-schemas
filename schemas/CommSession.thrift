namespace java org.cweb.schemas.comm.session

include "Wire.thrift"
include "Crypto.thrift"

struct CommSessionSeed {
  1: required binary sessionId
  2: required i64 generatedAt
  3: required Crypto.X3DHInitialMessage x3dhInitialMessage
  4: required Crypto.DoubleRatchetMessage initialDoubleRatchetMessage
}

struct CommSessionSeedLocalMetadata {
  1: required binary destId
  2: required binary sessionId
  3: required i64 generatedAt
}

struct CommSessionSeedCryptoEnvelope {
  1: required binary cryptoEnvelope
}

struct LocalCommSessionOutstandingMessage {
  1: required i32 serial
  2: required binary name
  3: required i64 deleteAt
}

struct LocalCommSessionState {
  1: required binary sessionId
  2: required i64 generatedAt
  3: required Crypto.DoubleRatchetState doubleRatchetState
  4: required i32 nextSerialOwn
  5: required i32 nextSerialRemote
  6: required i32 nextToAckByRemote
  7: required i32 nextRemoteSerialToAck
  8: required i64 lastReceivedAt
  9: required i64 lastForwardCheckTime
  10: required i64 lastLostCheckTime
  11: optional binary nextMessageNameOwn
  12: optional binary nextMessageNameRemote
  20: required list<LocalCommSessionOutstandingMessage> outstandingMessages = []
  21: required list<i32> unconsumedMessageSerials = []
  26: required list<i32> unconsumedAckedMessageSerials = []
}

struct CommSessionMessage {
  1: required i32 serial
  2: required i64 timestamp
  4: optional binary refId
  5: required i32 ackSerial
  6: optional binary nextMessageName
  20: required Wire.TypedPayload payload
}

struct LocalCommSessionMessage {
  1: required i64 createdAt
  2: optional i64 consumedAt
  3: optional i64 ackedAt
  20: required CommSessionMessage message
}

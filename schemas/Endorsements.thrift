namespace java org.cweb.schemas.endorsements

include "Properties.thrift"
include "Storage.thrift"

///////////////  Endorsements //////////////////
struct EndorsementPayload {
  20: required list<Properties.Property> properties;
}

///////////////  Endorsement request protocol //////////////////
struct EndorsementRequest {
  1: required binary requesterId
  2: required binary requestId
  20: required binary endorsementRequestPayload
}

struct EndorsementResponse {
  1: required binary endorserId
  2: required binary requestId
  4: optional string errorStr
  20: optional binary endorsementEnvelope  // signed CryptoEnvelope where payload is EndorsementPayload
}

struct EndorsementRequestLocalMetadata {
  1: required binary endorserId
  2: required binary requestId
  3: required binary payloadHash
}

struct EndorsementResponseLocalMetadata {
  1: required binary endorserId
  2: required binary requestId
  3: required binary payloadHash
}

namespace java org.cweb.schemas.keys

enum KeyType {
    RSA_2048,
    EC25519_256
}

struct KeyPair {
  1: required KeyType type
  2: required binary publicKey
  3: required binary privateKey
}

struct PublicKey {
  1: required KeyType type
  2: required binary publicKey
}

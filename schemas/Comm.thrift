namespace java org.cweb.schemas.comm

enum SessionType {
  COMM_SESSION = 1
  SHARED_SESSION = 2
}

struct SessionId {
  1: required SessionType type
  2: required binary id
}

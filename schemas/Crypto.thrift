namespace java org.cweb.schemas.crypto

include "Keys.thrift"

// X3DH
struct X3DHPreKeyBundle {
  1: optional binary idPublicKey  // Optional to save space, when known from the context
  2: required binary preKey
  3: required binary signature
}

struct X3DHInitialMessage {
  1: optional binary idPublicKey  // Optional to save space, when known from the context
  2: required binary preKeyHash
  3: required binary ephemeralPublicKey
  4: required binary secretKeyHash
}

// Double Ratchet
struct DoubleRatchetSkippedMessageKey {
  1: required binary headerKeyRemote
  2: required i32 messageSerialRemote
  3: required binary messageKeyRemote
}

struct DoubleRatchetState {
  1: required Keys.KeyPair dhKeyPairSelf
  2: binary dhPublicKeyRemote
  3: required binary rootKey
  4: binary chainKeySelf
  5: binary chainKeyRemote

  6: required i32 messageSerialSelf
  7: required i32 messageSerialRemote
  8: required i32 messageSerialPrevChainSelf

  9:  binary headerKeySelf
  10: binary headerKeyRemote
  11: required binary nextHeaderKeySelf
  12: required binary nextHeaderKeyRemote
  13: required list<DoubleRatchetSkippedMessageKey> skippedMessageKeys
}

struct DoubleRatchetMessage {
  2: required binary encryptedHeader
  3: required binary encryptedData
}

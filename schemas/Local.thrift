namespace java org.cweb.schemas.local

include "Keys.thrift"

struct IdentityKeys {
  1: required binary id
  3: required Keys.KeyPair rsaKey
  4: required Keys.KeyPair ecKey
}

enum LocalEndorsementTimestampType {
  REQUEST = 0
  RESPONSE = 1
}

struct LocalEndorsementProcessingTimestamp {
  1: required LocalEndorsementTimestampType type
  2: required i64 hash
  3: required i64 time
}

struct LocalEndorsementState {
  1: required list<LocalEndorsementProcessingTimestamp> timestamps = []
}

struct LocalPreKeyPair {
  1: required i64 time
  2: required binary publicKeyHash
  3: required Keys.KeyPair keyPair
}

struct LocalPreKeyState {
  1: required list<LocalPreKeyPair> keyPairs = []
}
